//! examples/01.rs
//! Example 01: An app with no widgets

use triage_widget::WidgetManager;

pub struct App {
    _widget_manager: WidgetManager,
}
impl App {
    pub fn new<S: Into<String>>(_args_raw: S) -> Self {
        Self { _widget_manager: WidgetManager {} }
    }
    pub fn try_to(&mut self, _command: &str) -> Result<String, String> {
        Err(String::from("No widgets, so no commands!"))
    }
}

fn main() {
    // Create a basic App, which has no Widgets.
    let mut app = App::new("foo.bypass=false foo.bypass=true");

    println!("\n> foo");
    let result = format!("{:?}", app.try_to("no_such_command"));
    assert_eq!(result, "Err(\"No widgets, so no commands!\")");
    println!("{}", result);
}
