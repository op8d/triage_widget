# TRIAGE Widget

__Encapsulates your app’s functionality into manageable chunks.__

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage_widget/>  
▶&nbsp; __Homepage:__ <https://op8d.gitlab.io/triage_widget/>  

Build and open the documentation:  
`cargo doc --no-deps --open`

## Examples

### Example 01: An app with no widgets
   `cargo run --example 01`
